package com.example.demo.jpa.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.example.demo.jpa.User;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    void testSaveUser() {
        User user = userRepository.save(User.builder()
                                            .name("John Doe")
                                            .email("test@hotmail.com").build());
        assertNotNull(user);
        List<User> users = userRepository.findAll();
        System.out.println(users);
        assertEquals(1, users.size());

    }


}