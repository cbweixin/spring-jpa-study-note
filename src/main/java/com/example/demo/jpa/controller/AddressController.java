package com.example.demo.jpa.controller;

import com.example.demo.jpa.Address;
import com.example.demo.jpa.dao.AddressRepository;
import com.example.demo.jpa.dao.UserRepository;
import com.example.demo.jpa.dto.AddressDto;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class AddressController {

    private final AddressRepository addressRepository;
    private final UserRepository userRepository;

    @Autowired
    public AddressController(AddressRepository addressRepository, UserRepository userRepository) {
        this.addressRepository = addressRepository;
        this.userRepository = userRepository;
    }


    @PostMapping(value = "/addresses", consumes = "application/json", produces = "application/json")
    public Address addNewUser(@RequestBody Address address) {
        var user = userRepository.findById(address.getUser().getId())
                               .orElseThrow(() -> new EntityNotFoundException("User not found with id: " + address.getUser().getId()));
        address.setUser(user);

        return addressRepository.save(address);
    }

    @PutMapping(value = "/address/{id}", consumes = "application/json", produces = "application/json")
    public Address updateAddress(@PathVariable Long id, @RequestBody AddressDto updatedAddress) {
        var user = userRepository.findById(updatedAddress.getUserId())
                                 .orElseThrow(() -> new EntityNotFoundException("User not found with id: " + updatedAddress.getUserId()));
        return addressRepository.findById(id)
                                .map(address -> {
                                    address.setStreet(updatedAddress.getStreet());
                                    address.setCity(updatedAddress.getCity());
                                    address.setZipCode(updatedAddress.getZipCode());
                                    address.setUser(user);
                                    return addressRepository.save(address);
                                })
                                .orElseThrow(() -> new EntityNotFoundException("Address not found with id: " + id));
    }


}
