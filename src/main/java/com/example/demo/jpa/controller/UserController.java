package com.example.demo.jpa.controller;

import com.example.demo.jpa.User;
import com.example.demo.jpa.dao.UserBaseRepository;
import com.example.demo.jpa.dao.UserPagingSortingRepository;
import com.example.demo.jpa.dao.UserRepository;
import com.example.demo.jpa.dao.UserSimplRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class UserController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserSimplRepository userRepository2;
    @Autowired
    private UserPagingSortingRepository userRepository3;
    @Autowired
    private UserBaseRepository userRepository4;

    @PostMapping(value = "/users", consumes = "application/json", produces = "application/json")
    public User addNewUser(@RequestBody User user) {
        return userRepository.save(user);
    }

    @PutMapping(value = "/user", consumes = "application/json", produces = "application/json")
    public User updateNewUser(@RequestBody User user) {
        return userRepository.save(user);
    }

    @GetMapping(value = "/users", produces = "application/json")
    @ResponseBody
    public Page<User> getAllUsers(Pageable request) {
        return userRepository.findAll(request);
    }

    @GetMapping(value = "/usersname", produces = "application/json")
    @ResponseBody
    public Page<User> getUsersByName(@RequestParam String name, Pageable request) {
        return userRepository2.findByName(name, request);
    }

    @GetMapping(value = "/userid", produces = "application/json")
    @ResponseBody
    public User getUsersByName(@RequestParam String id) {
        return userRepository4.findById(Long.parseLong(id)).get();
    }


    @GetMapping(value = "/user/address/{id}", produces = "application/json")
    @ResponseBody
    public User getUsersByAddress(@PathVariable String id) {
        return userRepository.findUserByAddressId(Long.parseLong(id));
    }

    @GetMapping(value = "/useremail", produces = "application/json")
    @ResponseBody
    public Page<User> getUsersByEmail(@RequestParam String email, Pageable request) {
        return userRepository4.findByEmail(email, request);
    }

    @GetMapping(value = "/page", produces = "application/json")
    @ResponseBody
    public Page<User> getAllUsersBypage() {
        return userRepository3.findAll(PageRequest.of(0, 20, Sort.by(new Sort.Order(Direction.ASC, "name"))));
    }


    @GetMapping(value = "/sort", produces = "application/json")
    @ResponseBody
    public Iterable<User> getAllUsersBySort() {
        return userRepository3.findAll(Sort.by(new Sort.Order(Direction.ASC, "name")));
    }
}
