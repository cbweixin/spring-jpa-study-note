package com.example.demo.jpa.dto;

import java.util.Objects;

public class AddressDto {
    private String street;
    private String city;
    private Long userId;
    private String zipCode;

    public String getStreet() {
        return street;
    }

    public AddressDto setStreet(String street) {
        this.street = street;
        return this;
    }

    public String getCity() {
        return city;
    }

    public AddressDto setCity(String city) {
        this.city = city;
        return this;
    }

    public Long getUserId() {
        return userId;
    }

    public AddressDto setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public String getZipCode() {
        return zipCode;
    }

    public AddressDto setZipCode(String zipCode) {
        this.zipCode = zipCode;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AddressDto that = (AddressDto) o;
        return Objects.equals(getStreet(), that.getStreet()) && Objects.equals(getCity(), that.getCity())
            && Objects.equals(getUserId(), that.getUserId()) && Objects.equals(getZipCode(), that.getZipCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getStreet(), getCity(), getUserId(), getZipCode());
    }

    @Override
    public String toString() {
        return "AddressDto{" + "street='" + street + '\'' + ", city='" + city + '\'' + ", userId=" + userId
            + ", zipCode='" + zipCode + '\'' + '}';
    }
}
