package com.example.demo.jpa.dao;

import com.example.demo.jpa.Address;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long>{

    Optional<Address> findByUserId(Long userId);

}
