package com.example.demo.jpa.dao;

import com.example.demo.jpa.User;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserPagingSortingRepository extends PagingAndSortingRepository<User, Long>{

}
