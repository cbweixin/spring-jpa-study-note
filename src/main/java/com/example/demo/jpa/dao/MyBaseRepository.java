package com.example.demo.jpa.dao;

import java.io.Serializable;
import java.util.Optional;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

@NoRepositoryBean
public interface MyBaseRepository<T, ID extends Serializable> extends Repository<T, ID>{

    <S extends T> S save(S entity);

    Optional<T> findById(ID id);

}
