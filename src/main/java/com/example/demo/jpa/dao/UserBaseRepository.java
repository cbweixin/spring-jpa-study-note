package com.example.demo.jpa.dao;

import com.example.demo.jpa.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserBaseRepository extends MyBaseRepository<User, Long>{
    Page<User> findByEmail(String email, Pageable pageable);
}
