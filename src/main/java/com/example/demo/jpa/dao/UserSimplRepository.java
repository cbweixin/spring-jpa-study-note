package com.example.demo.jpa.dao;

import com.example.demo.jpa.User;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.Repository;

public interface UserSimplRepository extends Repository<User, Long>{
    Page<User> findByName(String name, Pageable pageable);
    List<User> findByEmail(String email);
    List<User> findByEmailAndName(String email, String name);

}
