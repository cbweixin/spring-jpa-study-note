package com.example.demo.jpa.dao;

import com.example.demo.jpa.User;
import org.springframework.data.repository.Repository;

public interface PersonRepository extends Repository<User, Long>{

}
